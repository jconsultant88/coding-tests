#Project
Starling round up exercise

##Dependencies
- Jackson library to marshal and unmarshal json
- spring web library to use its rest template class for making rest calls
- spring core and context libraries to use it dependency injection to make code cleaner and to effective unit tests
- junit for unit testing
- mockito library for mocking services/components
- spring test to use it refection features and set properties to bean

##Test Coverage
Decent test coverage because of time limitation

##Running the program
```sh
$ <go to the project path>
$ mvn package
$ mvn exec:java
``` 

http://zetcode.com/spring/annotationconfigapplicationcontext/

https://github.com/ducey/RoundUps-to-Goal/blob/master/roundupsToGoal.php



