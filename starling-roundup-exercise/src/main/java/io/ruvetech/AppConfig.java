package io.ruvetech;

import io.ruvetech.rest.RestClient;
import io.ruvetech.rest.RestClientImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Bean
    public RestClient getRestClient() {
        return new RestClientImpl();
    }

    @Bean
    public RestTemplate getRestOperationst() {
        System.out.println("RestOperations instance created");
        return new RestTemplate();
    }
}
