package io.ruvetech;

import io.ruvetech.model.SavingsGoalRequest;
import io.ruvetech.model.SavingsGoalResponse;
import io.ruvetech.model.Transaction;
import io.ruvetech.model.Transactions;
import io.ruvetech.rest.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.ruvetech.util.Utils.isDateTimeWithInDays;

@Component
public class Application {

    @Autowired
    RestClient restClient;

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.scan("io.ruvetech");
        context.register(AppConfig.class);
        context.refresh();

        Application application = context.getBean(Application.class);
        application.run();

    }

    public void run() {
        try {
            Transactions transactions = restClient.getTransactions();

            List<Transaction> transactionList = transactions.getEmbedded().getTransactions();

            // filter transactions by current week
            transactionList = transactionList.stream().filter(transaction ->
                    isDateTimeWithInDays(transaction.getCreated(), 7)).
                    collect(Collectors.toCollection(ArrayList::new));

            Function<Double, Double> roundingFunction = amount -> {
                //logger.info("amount " + amount);
                String decimalValue = amount.toString().split("\\.")[1];
                BigDecimal bigDecimalPart = new BigDecimal("." + decimalValue);
                if (bigDecimalPart.doubleValue() == 0.00) {
                    //logger.info("-- No processing needed");
                    return 0.00;
                }
                BigDecimal result = new BigDecimal("1.00").subtract(new BigDecimal("." + decimalValue));
                //logger.info("result " + result.doubleValue());
                return result.doubleValue();
            };

            //filter out INBOUND transactions
            //map only amounts
            //apply rounding function
            //sum
            double finalAmount = transactionList.stream().
                    filter((transaction) -> transaction.getAmount() < 0).
                    map(Transaction::getAmount).
                    map(roundingFunction).
                    mapToDouble(d -> d).
                    sum();

            int amountInUnits = (int) finalAmount * 100;
            String currency = Currency.getInstance(Locale.getDefault()).getCurrencyCode();
            String name = "vacation-savings";
            SavingsGoalRequest.Target target = new SavingsGoalRequest.Target(currency, amountInUnits);
            SavingsGoalRequest savingsGoalRequest = new SavingsGoalRequest(name, currency, target);

            SavingsGoalResponse savingsGoalResponse = restClient.createSavingsGoal(savingsGoalRequest);
            restClient.addMoney(savingsGoalResponse.getSavingsGoalUid(), savingsGoalRequest);
            logger.info("final amount " + finalAmount);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
