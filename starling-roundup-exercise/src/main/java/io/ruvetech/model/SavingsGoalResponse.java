package io.ruvetech.model;

import java.util.Arrays;

public class SavingsGoalResponse {

    private String savingsGoalUid;

    private boolean success;

    private Error[] errors;

    public SavingsGoalResponse() {
    }

    public SavingsGoalResponse(String savingsGoalUid, boolean success, Error[] errors) {
        this.savingsGoalUid = savingsGoalUid;
        this.success = success;
        this.errors = errors;
    }

    public String getSavingsGoalUid() {
        return savingsGoalUid;
    }

    public void setSavingsGoalUid(String savingsGoalUid) {
        this.savingsGoalUid = savingsGoalUid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Error[] getErrors() {
        return errors;
    }

    public void setErrors(Error[] errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "SavingsGoalResponse{" +
                "savingsGoalUid='" + savingsGoalUid + '\'' +
                ", success=" + success +
                ", errors=" + Arrays.toString(errors) +
                '}';
    }

    public static class Error {
        private String message;

        public Error() {
        }

        public Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
