package io.ruvetech.model;

import java.util.Arrays;

public class AddMoneyResponse {

    private String transferUid;

    private boolean success;

    private Error[] errors;

    public AddMoneyResponse() {
    }

    public AddMoneyResponse(String transferUid, boolean success, Error[] errors) {
        this.transferUid = transferUid;
        this.success = success;
        this.errors = errors;
    }

    public String getTransferUid() {
        return transferUid;
    }

    public void setTransferUid(String transferUid) {
        this.transferUid = transferUid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Error[] getErrors() {
        return errors;
    }

    public void setErrors(Error[] errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "SavingsGoalResponse{" +
                "transferUid='" + transferUid + '\'' +
                ", success=" + success +
                ", errors=" + Arrays.toString(errors) +
                '}';
    }

    public static class Error {
        private String message;

        public Error() {
        }

        public Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
