package io.ruvetech.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface AddMoneyResponseMixins {

    @JsonProperty("transferUid")
    String getSavingsGoalUid();
}
