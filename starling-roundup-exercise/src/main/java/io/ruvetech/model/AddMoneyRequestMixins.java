package io.ruvetech.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface AddMoneyRequestMixins {

    @JsonProperty("amount")
    SavingsGoalRequest.Target getTarget();
}
