package io.ruvetech.model;

import java.util.Objects;

public class SavingsGoalRequest {

    private String name;

    private String currency;

    private Target target;

    public SavingsGoalRequest() {
    }

    public SavingsGoalRequest(String name, String currency, Target target) {
        this.name = name;
        this.currency = currency;
        this.target = target;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        hash = 31 * hash + (currency == null ? 0 : currency.hashCode());
        hash = 31 * hash + (target == null ? 0 : target.hashCode());

        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        SavingsGoalRequest savingsGoalRequest = (SavingsGoalRequest) o;
        // field comparison
        return Objects.equals(name, savingsGoalRequest.name)
                && Objects.equals(currency, savingsGoalRequest.currency)
                && Objects.equals(target, savingsGoalRequest.target);
    }


    @Override
    public String toString() {
        return "SavingsGoalRequest{" +
                "name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                ", target=" + target +
                '}';
    }

    public static class Target {

        private String currency;
        private int minorUnits;

        public Target() {
        }

        public Target(String currency, int minorUnits) {
            this.currency = currency;
            this.minorUnits = minorUnits;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getMinorUnits() {
            return minorUnits;
        }

        public void setMinorUnits(int minorUnits) {
            this.minorUnits = minorUnits;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 31 * hash + minorUnits;
            hash = 31 * hash + (currency == null ? 0 : currency.hashCode());

            return hash;
        }

        @Override
        public boolean equals(Object o) {
            // self check
            if (this == o)
                return true;
            // null check
            if (o == null)
                return false;
            // type check and cast
            if (getClass() != o.getClass())
                return false;
            Target target = (Target) o;
            // field comparison
            return Objects.equals(currency, target.currency)
                    && Objects.equals(minorUnits, target.minorUnits);
        }

        @Override
        public String toString() {
            return "Target{" +
                    "currency='" + currency + '\'' +
                    ", minorUnits=" + minorUnits +
                    '}';
        }
    }
}


