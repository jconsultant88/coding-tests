package io.ruvetech.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.ruvetech.model.AddMoneyResponse;
import io.ruvetech.model.SavingsGoalRequest;
import io.ruvetech.model.SavingsGoalResponse;
import io.ruvetech.model.Transactions;
import io.ruvetech.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;

public class RestClientImpl implements RestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${access.token}")
    private String bearerToken;

    @Value("${base.uri}")
    private String baseUri;

    @Value("${transactions.uri}")
    private String transactionsURI;

    @Value("${savings.uri}")
    private String savingUri;

    @Value("${addMoney.uri}")
    private String addMoneyUri;

    @Value("${user.agent}")
    private String userAgent;

    private static final Logger logger = LoggerFactory.getLogger(RestClientImpl.class);

    @PostConstruct
    public void afterInitilisation() {
        bearerToken = "Bearer " + bearerToken;
    }

    @Override
    public Transactions getTransactions() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, getStandardHeaders());
        try {

            ResponseEntity<Transactions> result = restTemplate.exchange(transactionsURI, HttpMethod.GET, entity, Transactions.class);

            return result.getBody();
        } catch (HttpClientErrorException ex) {
            logger.error("HttpClientErrorException occurred :" + ex);
            throw new Exception(ex);
        }
    }

    @Override
    public SavingsGoalResponse createSavingsGoal(SavingsGoalRequest savingsGoalRequest) throws Exception {
        try {
            HttpEntity entity = new HttpEntity<>(savingsGoalRequest, getStandardHeaders());
            ResponseEntity<SavingsGoalResponse> response = restTemplate.exchange(savingUri, HttpMethod.PUT, entity, SavingsGoalResponse.class);
            logger.info("Object returned :" + response);
            return response.getBody();
        } catch (HttpClientErrorException ex) {
            logger.error("HttpClientErrorException occurred :" + ex);
            throw new Exception(ex);
        }
    }

    @Override
    public AddMoneyResponse addMoney(String savingsGoalUid, SavingsGoalRequest savingsGoalRequest) throws Exception {
        StringBuilder stringBuilder = new StringBuilder(savingUri);
        stringBuilder.append(savingsGoalUid);
        stringBuilder.append(addMoneyUri);
        stringBuilder.append(Utils.getRandomTransferUid());

        String uri = stringBuilder.toString();
        HttpHeaders httpHeaders = getStandardHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        try {
            String jsonContent = Utils.createAddMoneyJsonRequest(savingsGoalRequest.getName(),
                    savingsGoalRequest.getTarget().getMinorUnits(), savingsGoalRequest.getCurrency());
            HttpEntity<String> entity = new HttpEntity<String>(jsonContent, httpHeaders);
            ResponseEntity<AddMoneyResponse> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, AddMoneyResponse.class);

            logger.info("Object returned :" + response.getBody());
            return response.getBody();
        } catch (HttpClientErrorException ex) {
            logger.error("HttpClientErrorException occurred :" + ex);
            throw new Exception(ex);
        } catch (JsonProcessingException ex) {
            logger.error("JsonProcessingException occurred :" + ex);
            throw new Exception(ex);
        }
    }

    public HttpHeaders getStandardHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("User-Agent", userAgent);
        headers.add("Authorization", bearerToken);

        return headers;
    }


}
