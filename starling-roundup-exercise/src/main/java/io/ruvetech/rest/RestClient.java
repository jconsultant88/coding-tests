package io.ruvetech.rest;

import io.ruvetech.model.AddMoneyResponse;
import io.ruvetech.model.SavingsGoalRequest;
import io.ruvetech.model.SavingsGoalResponse;
import io.ruvetech.model.Transactions;

public interface RestClient {

    Transactions getTransactions() throws Exception;

    SavingsGoalResponse createSavingsGoal(SavingsGoalRequest savingsGoalRequest) throws Exception;

    AddMoneyResponse addMoney(String savingsGoalUid, SavingsGoalRequest savingsGoalRequest) throws Exception;
}
