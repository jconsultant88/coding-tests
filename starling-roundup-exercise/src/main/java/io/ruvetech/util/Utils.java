package io.ruvetech.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.ruvetech.model.AddMoneyRequestMixins;
import io.ruvetech.model.SavingsGoalRequest;

import java.time.*;
import java.util.UUID;

public class Utils {

    /**
     * Checks if the date given is below given 'numberOfDays'.<br/>
     * Date given is assumed to be in UTC format
     *
     * @param date         in string format
     * @param numberOfDays number of days before the
     */
    public static boolean isDateTimeWithInDays(final String date, final int numberOfDays) {
        Instant instant = Instant.parse(date);

        LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));

        LocalDateTime oldDate = LocalDateTime.now(Clock.systemUTC()).minusDays(numberOfDays);

        return result.isAfter(oldDate);
    }

    public static String getRandomTransferUid() {
        return UUID.randomUUID().toString();
    }

    public static String createAddMoneyJsonRequest(String name, int amountInUnits, String currency) throws JsonProcessingException {
        SavingsGoalRequest.Target target = new SavingsGoalRequest.Target(currency, amountInUnits);
        SavingsGoalRequest savingsGoalRequest = new SavingsGoalRequest(name, currency, target);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.addMixIn(SavingsGoalRequest.class, AddMoneyRequestMixins.class);

        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savingsGoalRequest);
    }
}
