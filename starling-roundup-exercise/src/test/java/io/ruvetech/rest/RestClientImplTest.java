package io.ruvetech.rest;

import io.ruvetech.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RestClientImplTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private RestClientImpl restClient = new RestClientImpl();

    @Mock
    ResponseEntity<Transactions> transactionsResponseEntity;

    private final String CURRENCY = "YYY";

    @Before
    public void beforeEveryTest() {
        ReflectionTestUtils.setField(restClient, "transactionsURI", "http://foo");
        ReflectionTestUtils.setField(restClient, "savingUri", "http://foo");
        ReflectionTestUtils.setField(restClient, "addMoneyUri", "http://foo");
    }

    @After
    public void afterEveryTest() {
        //do nothing
    }

    @Test
    public void getTransactionsTest() throws Exception {
        //Given
        Transactions transactions = getTransactions();

        when(restTemplate.exchange(nullable(String.class), any(HttpMethod.class),
                any(), ArgumentMatchers.<Class<Transactions>>any())).thenReturn(transactionsResponseEntity);

        when(transactionsResponseEntity.getBody()).thenReturn(transactions);

        //When
        Transactions restClientTransactions = restClient.getTransactions();

        //Then
        assertNotNull(restClientTransactions);
        assertEquals(transactions.getEmbedded().getTransactions().get(0).getAmount(), restClientTransactions.getEmbedded().getTransactions().get(0).getAmount());
    }

    @Test(expected = Exception.class)
    public void getTransactionsThrowsException() throws Exception {
        //Given
        Transactions transactions = getTransactions();

        when(restTemplate.exchange(ArgumentMatchers.any(String.class), any(HttpMethod.class),
                any(), ArgumentMatchers.<Class<Transactions>>any())).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        //When
        Transactions restClientTransactions = restClient.getTransactions();

        //Then
        fail("should have not reached here");

    }

    @Test
    public void createSavingsGoalTest() throws Exception {
        //Given
        ReflectionTestUtils.setField(restClient, "savingUri", "http://foo");
        SavingsGoalRequest savingsGoalRequest = createSavingsGoalRequest();
        HttpEntity<SavingsGoalRequest> entity = new HttpEntity<SavingsGoalRequest>(savingsGoalRequest, restClient.getStandardHeaders());
        SavingsGoalResponse savingsGoalResponse = new SavingsGoalResponse("tatjahahahah", true, null);

        ResponseEntity<SavingsGoalResponse> responseEntity = mock(ResponseEntity.class);

        when(restTemplate.exchange(ArgumentMatchers.any(String.class),
                ArgumentMatchers.eq(HttpMethod.PUT),
                ArgumentMatchers.any(HttpEntity.class),
                ArgumentMatchers.<Class<SavingsGoalResponse>>any())).thenReturn(responseEntity);
        when(responseEntity.getBody()).thenReturn(savingsGoalResponse);

        //When
        SavingsGoalResponse savingsGoalResponse1 = restClient.createSavingsGoal(savingsGoalRequest);

        //Then
        assertNotNull(savingsGoalResponse1);
        assertEquals(savingsGoalResponse.getSavingsGoalUid(), savingsGoalResponse1.getSavingsGoalUid());
        assertNull(savingsGoalResponse1.getErrors());
    }

    @Test(expected = Exception.class)
    public void createSavingsGoalTestThrowsException() throws Exception {
        //Given
        SavingsGoalRequest savingsGoalRequest = createSavingsGoalRequest();
        HttpEntity<SavingsGoalRequest> entity = new HttpEntity<SavingsGoalRequest>(savingsGoalRequest, restClient.getStandardHeaders());
        SavingsGoalResponse savingsGoalResponse = new SavingsGoalResponse("tatjahahahah", true, null);

        ResponseEntity<SavingsGoalResponse> responseEntity = mock(ResponseEntity.class);

        when(restTemplate.exchange(ArgumentMatchers.any(String.class),
                ArgumentMatchers.eq(HttpMethod.PUT),
                ArgumentMatchers.any(HttpEntity.class),
                ArgumentMatchers.<Class<SavingsGoalResponse>>any())).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));
        //When
        SavingsGoalResponse savingsGoalResponse1 = restClient.createSavingsGoal(savingsGoalRequest);

        //Then
        fail("should have not reached here");
    }

    @Test
    public void addMoneyTest() throws Exception {
        //Given
        ReflectionTestUtils.setField(restClient, "savingUri", "http://foo");
        SavingsGoalRequest savingsGoalRequest = createSavingsGoalRequest();
        AddMoneyResponse addMoneyResponse = new AddMoneyResponse("tatjahahahah", true, null);

        ResponseEntity<AddMoneyResponse> responseEntity = mock(ResponseEntity.class);

        when(restTemplate.exchange(ArgumentMatchers.any(String.class),
                ArgumentMatchers.eq(HttpMethod.PUT),
                ArgumentMatchers.any(HttpEntity.class),
                ArgumentMatchers.<Class<AddMoneyResponse>>any())).thenReturn(responseEntity);

        when(responseEntity.getBody()).thenReturn(addMoneyResponse);

        //When
        AddMoneyResponse addMoneyResponse1 = restClient.addMoney("savingsGoalUid", savingsGoalRequest);

        //Then
        assertNotNull(addMoneyResponse1);
        assertEquals(addMoneyResponse1.getTransferUid(), addMoneyResponse1.getTransferUid());
        assertNull(addMoneyResponse1.getErrors());
    }


    @Test(expected = Exception.class)
    public void addMoneyTestThrowsException() throws Exception {
        //Given
        SavingsGoalRequest savingsGoalRequest = createSavingsGoalRequest();
        AddMoneyResponse addMoneyResponse = new AddMoneyResponse("tatjahahahah", true, null);

        ResponseEntity<AddMoneyResponse> responseEntity = mock(ResponseEntity.class);

        when(restTemplate.exchange(ArgumentMatchers.any(String.class),
                ArgumentMatchers.eq(HttpMethod.PUT),
                ArgumentMatchers.any(HttpEntity.class),
                ArgumentMatchers.<Class<AddMoneyResponse>>any())).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        //When
        AddMoneyResponse addMoneyResponse1 = restClient.addMoney("savingsGoalUid", savingsGoalRequest);

        //Then
        fail("should have not reached here");
    }

    private Transactions getTransactions() {
        int length = 3;
        List<Transaction> transactionList = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            Transaction transaction = new Transaction();
            transaction.setAmount(new Double(i));
            transaction.setSource("Test");
            transactionList.add(transaction);
        }

        Embedded embedded = new Embedded();
        embedded.setTransactions(transactionList);
        Transactions transactions = new Transactions();
        transactions.setEmbedded(embedded);

        return transactions;
    }

    private SavingsGoalRequest createSavingsGoalRequest() {
        SavingsGoalRequest.Target target = new SavingsGoalRequest.Target(CURRENCY, 11);
        SavingsGoalRequest savingsGoalRequest = new SavingsGoalRequest("account-name", CURRENCY, target);
        return savingsGoalRequest;
    }
}
