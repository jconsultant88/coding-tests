package io.ruvetech.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ModelTests {

    private ObjectMapper objectMapper;
    private static final String CURRENCY = "BitCoin";
    private static final String SAVINGS_NAME = "Savings...Really!";

    @Before
    public void beforeEveryTest() {
        objectMapper = new ObjectMapper();
    }

    @After
    public void afterEveryTest() {
        //do nothing
    }

    @Test
    public void unMarshalToTransactions() throws IOException {
        //Given
        String jsonString = new String(Files.readAllBytes(Paths.get("src/test/resources/transactions.json")));

        //When
        Transactions transactions = objectMapper.readValue(jsonString, Transactions.class);

        //Then
        assertNotNull(transactions);
    }

    @Test
    public void marshalSavingsGoal() throws IOException {
        //Given

        SavingsGoalRequest savingsGoalRequest = new SavingsGoalRequest(SAVINGS_NAME, CURRENCY, null);
        SavingsGoalRequest.Target target = new SavingsGoalRequest.Target(CURRENCY, 11);
        savingsGoalRequest.setTarget(target);

        //When
        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savingsGoalRequest);

        //Then
        objectMapper = new ObjectMapper();
        SavingsGoalRequest savingsGoalRequest1 = objectMapper.readValue(jsonString, SavingsGoalRequest.class);
        assertEquals(CURRENCY, savingsGoalRequest1.getCurrency());

    }

    @Test
    public void marshalAddMoney() throws IOException {

        //Given
        SavingsGoalRequest.Target target = new SavingsGoalRequest.Target(CURRENCY, 11);
        SavingsGoalRequest savingsGoalRequest = new SavingsGoalRequest(SAVINGS_NAME, CURRENCY, target);
        objectMapper.addMixIn(SavingsGoalRequest.class, AddMoneyRequestMixins.class);

        //When
        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savingsGoalRequest);

        //Then
        objectMapper = new ObjectMapper();
        objectMapper.addMixIn(SavingsGoalRequest.class, AddMoneyRequestMixins.class);
        SavingsGoalRequest savingsGoalRequest1 = objectMapper.readValue(jsonString, SavingsGoalRequest.class);
        assertEquals(target.getCurrency(), savingsGoalRequest1.getTarget().getCurrency());

        System.out.println(UUID.randomUUID().toString());
    }

}
