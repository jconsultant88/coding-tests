package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.parental_control_service.ParentalControlService;
import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

/**
 * Tests ParentalControlService
 */

@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceTest {

    @Mock
    private MovieService movieService;

    @InjectMocks
    private ParentalControlService parentalControlService = new ParentalControlService();

    Map<String, String> movieMap = Stream.of(new String[][]{
            {"Finding Nemo", "U"},
            {"Frozen", "PG"},
            {"Interstellar", "12"},
            {"The Matrix", "15"},
            {"The Wolf of Wall Street", "18"},
    }).collect(Collectors.toMap(data -> data[0], data -> data[1], (u, v) -> v, LinkedHashMap::new));

    @Test
    public void testCanWatchMovieWhenParentalSettingsMatch() throws Exception {
        //Given
        boolean permissionFlag = false;
        Collection<String> movieTitles = movieMap.keySet();

        for (String movieTitle : movieTitles) {
            String customerParentalControlSetting = movieMap.get(movieTitle);
            when(movieService.getParentalControlLevel(movieTitle)).thenReturn(movieMap.get(movieTitle));

            //When
            permissionFlag = parentalControlService.canWatchMovie(customerParentalControlSetting, movieTitle);

            //Then
            verify(movieService, times(1)).getParentalControlLevel(movieTitle);
            Assert.assertTrue(permissionFlag);
        }
    }

    @Test
    public void testCanWatchMovieWhenCustomerSettingsAreLow() throws Exception {
        //Given
        String movieTitles[] = {"Finding Nemo", "Frozen", "Interstellar", "The Matrix", "The Wolf of Wall Street"};

        boolean persmissions[] = {true, false, false, false, false};
        int index = 0;
        String customerParentalControlSetting = "U";

        for (String movieTitle : movieTitles) {
            when(movieService.getParentalControlLevel(movieTitle)).thenReturn(movieMap.get(movieTitle));

            //When
            boolean isPermitted = parentalControlService.canWatchMovie(customerParentalControlSetting, movieTitle);

            //Then
            verify(movieService, times(1)).getParentalControlLevel(movieTitle);
            Assert.assertEquals("No permission to watch " + movieTitle, persmissions[index], isPermitted);
            index++;
        }
    }

    @Test
    public void testCanWatchMovieWhenCustomerSettingsAreHigh() throws Exception {
        //Given
        String movieTitles[] = {"Finding Nemo", "Frozen", "Interstellar", "The Matrix", "The Wolf of Wall Street"};

        boolean persmissions[] = {true, true, true, true, true};
        int index = 0;
        String customerParentalControlSetting = "18";

        for (String movieTitle : movieTitles) {
            when(movieService.getParentalControlLevel(movieTitle)).thenReturn(movieMap.get(movieTitle));

            //When
            boolean isPermitted = parentalControlService.canWatchMovie(customerParentalControlSetting, movieTitle);

            //Then
            verify(movieService, times(1)).getParentalControlLevel(movieTitle);
            Assert.assertEquals("No permission to watch " + movieTitle, persmissions[index], isPermitted);
            index++;
        }
    }

    @Test
    public void testCanWatchMovieWithDiffCombinations() throws Exception {
        testMovieConditions("PG", "Finding Nemo", true);
        testMovieConditions("PG", "Interstellar", false);
        testMovieConditions("PG", "The Matrix", false);
        testMovieConditions("PG", "The Wolf of Wall Street", false);
        testMovieConditions("12", "Finding Nemo", true);
        testMovieConditions("12", "Frozen", true);
        testMovieConditions("12", "The Matrix", false);
        testMovieConditions("12", "The Wolf of Wall Street", false);
        testMovieConditions("15", "Finding Nemo", true);
        testMovieConditions("15", "Frozen", true);
        testMovieConditions("15", "Interstellar", true);
        testMovieConditions("15", "The Wolf of Wall Street", false);
    }

    private void testMovieConditions(String customerParentalControlSetting, String movieTitle, boolean expected) throws Exception {
        when(movieService.getParentalControlLevel(movieTitle)).thenReturn(movieMap.get(movieTitle));
        //When
        boolean isPermitted = parentalControlService.canWatchMovie(customerParentalControlSetting, movieTitle);
        //Then
        verify(movieService, times(1)).getParentalControlLevel(movieTitle);
        Assert.assertEquals("No permission to watch " + movieTitle, expected, isPermitted);
        reset(movieService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCanWatchMovieWhenMovieTitleIsNull() throws Exception {
        //When
        parentalControlService.canWatchMovie(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCanWatchMovieWhenCustomerSettingsIsNull() throws Exception {
        //When
        parentalControlService.canWatchMovie(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCanWatchMovieWhenParentalSettingsDoesNotExist() throws Exception {
        //When
        parentalControlService.canWatchMovie("XX", "YYY");
    }

    @Test(expected = RuntimeException.class)
    public void testCanWatchMovieWhenTitleIsNotFound() throws Exception {
        String movieTitle = "ZZZ";
        //When
        when(movieService.getParentalControlLevel(movieTitle)).thenThrow(new TitleNotFoundException());
        parentalControlService.canWatchMovie("PG", movieTitle);

        verify(movieService, times(1)).getParentalControlLevel(movieTitle);
    }

    @Test(expected = RuntimeException.class)
    public void testCanWatchMovieWhenTechnicalFailureExceptionOccurs() throws Exception {
        String movieTitle = "ZZZ";
        //When
        when(movieService.getParentalControlLevel(movieTitle)).thenThrow(new TechnicalFailureException());
        parentalControlService.canWatchMovie("PG", movieTitle);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCanWatchMovieWhenMovieSettingsDoesNotExist() throws Exception {
        String movieTitle = "ZZZ";
        //When
        when(movieService.getParentalControlLevel(movieTitle)).thenReturn("ASDF");
        parentalControlService.canWatchMovie("PG", movieTitle);
    }
}
