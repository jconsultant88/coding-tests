package com.bskyb.internettv.data;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum ParentalControlSetting {

    U("U", 1),
    PG("PG", 2),
    P12("12", 3),
    P15("15", 4),
    P18("18", 5);

    ParentalControlSetting(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    private final String name;
    private final int value;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    static final Map<String, ParentalControlSetting> names = Arrays.stream(ParentalControlSetting.values())
            .collect(Collectors.toMap(ParentalControlSetting::getName, Function.identity()));
    static final Map<Integer, ParentalControlSetting> values = Arrays.stream(ParentalControlSetting.values())
            .collect(Collectors.toMap(ParentalControlSetting::getValue, Function.identity()));

    public static ParentalControlSetting fromName(final String name) {
        return names.get(name);
    }

    public static ParentalControlSetting fromValue(final int value) {
        return values.get(value);
    }
}
