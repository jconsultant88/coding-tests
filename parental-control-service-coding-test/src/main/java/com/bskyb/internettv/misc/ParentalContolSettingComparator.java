package com.bskyb.internettv.misc;

import com.bskyb.internettv.data.ParentalControlSetting;

import java.util.Comparator;

public class ParentalContolSettingComparator implements Comparator<ParentalControlSetting> {

    /**
     * returns positive , negative or 0
     * 0 - indicates both setting match
     * + - indicates customer setting exceeds movie setting
     * - - indicates customer setting subceed movie setting
     * @param cPCSetting
     * @param mPCSetting
     * @return
     */

    public int compare(ParentalControlSetting cPCSetting, ParentalControlSetting mPCSetting) {
        String movieSetting = mPCSetting.getName();
        String customerSetting = cPCSetting.getName();
        int result = movieSetting.compareTo(customerSetting);
        if (result == 0) {
            return 0;
        } else {
            return cPCSetting.getValue() - mPCSetting.getValue();
        }
    }
}
