package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.data.ParentalControlSetting;
import com.bskyb.internettv.misc.ParentalContolSettingComparator;
import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;

public class ParentalControlService {

    @Autowired
    MovieService movieService;

    public boolean canWatchMovie(String customerParentalControlLevel, String movieId) throws Exception {

        try {

            Validate.notNull(customerParentalControlLevel);
            Validate.notNull(movieId);
            ParentalControlSetting customerControlSetting = ParentalControlSetting.fromName(customerParentalControlLevel);

            Validate.notNull(customerControlSetting);

            String movingSettingStr = movieService.getParentalControlLevel(movieId);

            ParentalControlSetting movieControlSetting = ParentalControlSetting.fromName(movingSettingStr);

            Validate.notNull(movieControlSetting);

            int result = (new ParentalContolSettingComparator()).compare(customerControlSetting, movieControlSetting);

            return (result >= 0 ? true : false);

        } catch (TechnicalFailureException | TitleNotFoundException exception) {
            System.out.println(exception);
            throw new RuntimeException(exception);
        }

    }
}
