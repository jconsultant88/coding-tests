package com.amido.rest.service;

import com.amido.rest.data.EventfulData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class EventFulServiceTest {

    @Mock
    private RestTemplate restTemplate;

    private EventFulService eventFulService;

    @Mock
    ResponseEntity<EventfulData> eventfulDataResponseEntity;

    @Mock
    private EventfulData eventfulData;

    @Before
    public void setUp() {
        eventFulService = new EventFulService(restTemplate);
    }

    @Test
    public void getEventsDataOnSuccess() {

        when(restTemplate.exchange(nullable(String.class), HttpMethod.GET,
                any(), ArgumentMatchers.<Class<EventfulData>>any())).thenReturn(eventfulDataResponseEntity);

        when(eventfulDataResponseEntity.getBody()).thenReturn(eventfulData);

        //When
        EventfulData eventfulData1 = eventFulService.getEvents();

        //Then
        assertNotNull(eventfulData1);
        //TODO - more validations needs to be added

        //assertTrue(eventfulData1.equals(eventfulData));

    }

}