package com.amido.rest.controller;

import com.amido.rest.data.EventfulData;
import com.amido.rest.service.EventFulService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(EventfulController.class)
public class EventfulControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EventFulService eventFulService;

    private EventfulData eventfulData;

    @Before
    public void setup() {
        eventfulData = getData();
    }

    @Test
    public void testGetEventData() throws Exception {

        given(eventFulService.getEvents()).willReturn(eventfulData);

        mockMvc.perform(MockMvcRequestBuilders.get("/events"))
                .andExpect(status().isOk());
                //.andExpect(content().json(""));
    }


    private EventfulData getData() {
        //TODO - construct eventful data object that will be returned by mocked service
        return new EventfulData();
    }
}