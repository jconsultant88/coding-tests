package com.amido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmidoJavaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmidoJavaTestApplication.class, args);
	}

}
