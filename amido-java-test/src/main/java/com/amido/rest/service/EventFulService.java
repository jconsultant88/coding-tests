package com.amido.rest.service;

import com.amido.rest.data.EventfulData;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class EventFulService {

    private final RestTemplate restTemplate;

    //TODO - Move to property file
    //@Value("${eventsURI}")
    private String eventsURI = "http://api.eventful.com/json/events/search?app_key=PHV3rqv2ScXgNDTJ&location=London&category=musical";

    public EventFulService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public EventfulData getEvents() {

        HttpEntity<String> entity = new HttpEntity<>(null, getStandardHeaders());
        ResponseEntity<EventfulData> result = restTemplate.exchange(eventsURI, HttpMethod.GET, entity, EventfulData.class);

        return result.getBody();
    }


    public HttpHeaders getStandardHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
