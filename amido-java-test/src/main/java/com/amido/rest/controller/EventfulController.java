package com.amido.rest.controller;

import com.amido.rest.data.EventfulData;
import com.amido.rest.service.EventFulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventfulController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventfulController.class);

    private final EventFulService eventFulService;

    public EventfulController(EventFulService eventFulService) {
        this.eventFulService = eventFulService;
    }

    @GetMapping("/events")
    public void getEventData() {
        //TODO - create an exception handler for listening to business exceptions
        EventfulData eventfulData = eventFulService.getEvents();
        ResponseEntity.ok(eventfulData);
    }
}
