package io.ruvetech;

import io.ruvetech.domain.ApplicationUser;
import io.ruvetech.domain.ValidationError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IntegrationTest {

    private final LocalDate dob = LocalDate.of(2000, 8, 23);

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testRegister() {

        ApplicationUser applicationUser = new ApplicationUser("username", "1Password", "AAA-GG-SSSS", dob);
        ResponseEntity<ApplicationUser> responseEntity = testRestTemplate.postForEntity(
                "/register", applicationUser, ApplicationUser.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    }

    @Test
    public void testRegisterFailedWithAlreadyRegisteredError() {

        ApplicationUser applicationUser = new ApplicationUser("username", "1Password", "222-33-4444", dob);
        register(applicationUser);

        ResponseEntity<ValidationError> responseEntity = testRestTemplate.postForEntity(
                "/register", applicationUser, ValidationError.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getErrorMessage()).isEqualTo("user already registered");

    }

    @Test
    public void testRegisterWhenValidationFailed() {

        ApplicationUser applicationUser = new ApplicationUser("username", "password", "444-33-4444", dob);
        ResponseEntity<ValidationError> responseEntity = testRestTemplate.postForEntity(
                "/register", applicationUser, ValidationError.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        assertTrue(responseEntity.getBody().getErrorMessage().startsWith("Validation failed"));

    }

    private void register(ApplicationUser applicationUser) {
        testRestTemplate.postForEntity(
                "/register", applicationUser, ApplicationUser.class);
    }

}
