package io.ruvetech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.ruvetech.domain.ApplicationUser;
import io.ruvetech.exception.UserAlreadyRegisteredException;
import io.ruvetech.exception.UserBlackListedException;
import io.ruvetech.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static io.ruvetech.validation.UserValidator.PASSWORD_NOT_VALID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willThrow;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ApplicationUserController.class)
public class ApplicationUserControllerTest {

    private final LocalDate dob = LocalDate.of(2000, 8, 23);
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private UserService userService;
    private ApplicationUser applicationUser;
    private String jsonContent;

    @Before()
    public void setUp() throws Exception {
        applicationUser = new ApplicationUser("javacoder", "Test123", "AAA-GG-SSSS", dob);
        jsonContent = objectMapper.writeValueAsString(applicationUser);
    }

    @Test
    public void testUserRegistrationSuccess() throws Exception {

        willDoNothing().given(userService).createUser(applicationUser);

        mockMvc.perform(MockMvcRequestBuilders.post("/register").content(jsonContent).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, "/register"));
    }

    @Test
    public void testUserRegistrationFailedWhenUserIsBlackListed() throws Exception {

        applicationUser = new ApplicationUser("javacoder", "Test123", "111-11-1111", LocalDate.of(1980, 1, 1));
        jsonContent = objectMapper.writeValueAsString(applicationUser);

        willThrow(new UserBlackListedException("User is not allowed to register")).given(userService).createUser(any());

        mockMvc.perform(MockMvcRequestBuilders.post("/register").content(jsonContent).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("User is not allowed to register"));

    }

    @Test
    public void testUserRegistrationFailedWhenUserIsAlready() throws Exception {

        willThrow(new UserAlreadyRegisteredException()).given(userService).createUser(any());

        mockMvc.perform(MockMvcRequestBuilders.post("/register").content(jsonContent).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("user already registered"));
    }

    @Test
    public void testUserRegistrationFailedWhenPasswordIsNotValid() throws Exception {
        applicationUser = new ApplicationUser("javacoder", "te3", "AAA-GG-BBBBB", dob);
        jsonContent = objectMapper.writeValueAsString(applicationUser);
        mockMvc.perform(MockMvcRequestBuilders.post("/register").content(jsonContent).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("Validation failed. 1 error(s)"))
                .andExpect(jsonPath("$.errors[0]").value(PASSWORD_NOT_VALID));
    }

    @Test
    public void testUserRegistrationFailedWhenPayloadConversionFailed() throws Exception {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("username", "username");
        requestBody.put("password", "1password");
        requestBody.put("ssnNumber", "AAA-GG-SSSS");
        requestBody.put("dob", "1980-1-01");
        jsonContent = objectMapper.writeValueAsString(requestBody);
        mockMvc.perform(MockMvcRequestBuilders.post("/register").content(jsonContent).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("Could not parse '1980-1-01' to java.time.LocalDate"));

    }

}
