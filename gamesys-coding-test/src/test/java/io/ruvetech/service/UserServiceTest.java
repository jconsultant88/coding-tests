package io.ruvetech.service;

import io.ruvetech.domain.ApplicationUser;
import io.ruvetech.domain.CacheKey;
import io.ruvetech.entity.User;
import io.ruvetech.exception.UserAlreadyRegisteredException;
import io.ruvetech.exception.UserBlackListedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.time.LocalDate;

import static junit.framework.TestCase.fail;
import static org.mockito.BDDMockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private ExclusionService exclusionService;

    @Mock
    private CacheManager cacheManager;

    @Mock
    private Cache userCache;

    private UserService userService;

    private ApplicationUser applicationUser;

    private CacheKey cacheKey;

    @Before
    public void setUp() {
        userService = new UserService(exclusionService, cacheManager);

        given(cacheManager.getCache(anyString())).willReturn(userCache);

        userService.init();
        applicationUser = new ApplicationUser("javacoder", "Test123$", "AAA-GG-SSSS", LocalDate.of(2018, 7, 23));
        cacheKey = new CacheKey(applicationUser.getSsnNumber(), applicationUser.getDob().toString());
    }

    @Test
    public void createUser_successful() {

        given(exclusionService.validate(anyString(), anyString())).willReturn(true);
        given(userCache.get(cacheKey, User.class)).willReturn(null);
        willDoNothing().given(userCache).put(any(CacheKey.class), any(User.class));

        userService.createUser(applicationUser);

        then(exclusionService).should().validate(applicationUser.getSsnNumber(), applicationUser.getDob().toString());
        then(userCache).should().put(any(CacheKey.class), any(User.class));
    }


    @Test
    public void createUser_userBlacklisted() {

        given(exclusionService.validate(anyString(), anyString())).willReturn(false);

        try {
            userService.createUser(applicationUser);
            fail("should have not reached here");
        } catch (UserBlackListedException ex) {
            //gulp
        }

        then(userCache).should(never()).get(cacheKey, User.class);
        then(userCache).should(never()).put(any(CacheKey.class), any(User.class));
    }

    @Test
    public void createUser_alreadyExists() {

        User existingUser = new User(applicationUser.getUserName(), applicationUser.getPassword(), applicationUser.getSsnNumber(), applicationUser.getDob());
        given(exclusionService.validate(anyString(), anyString())).willReturn(true);
        given(userCache.get(cacheKey, User.class)).willReturn(existingUser);

        try {
            userService.createUser(applicationUser);
            fail("should have not reached here");
        } catch (UserAlreadyRegisteredException ex) {
            //gulp
        }

        then(userCache).should(never()).put(any(CacheKey.class), any(User.class));
    }

    @Test
    public void getUser_returnNull() {

        given(userCache.get(cacheKey, User.class)).willReturn(null);

        userService.getUser(applicationUser.getSsnNumber(), applicationUser.getDob());

        then(userCache).should().get(cacheKey, User.class);
    }

    @Test
    public void getUser_returnNonNull() {
        User existingUser = new User(applicationUser.getUserName(), applicationUser.getPassword(), applicationUser.getSsnNumber(), applicationUser.getDob());
        given(userCache.get(cacheKey, User.class)).willReturn(existingUser);

        userService.getUser(applicationUser.getSsnNumber(), applicationUser.getDob());

        then(userCache).should().get(cacheKey, User.class);
    }
}