package io.ruvetech.validation;

import io.ruvetech.domain.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.List;

import static io.ruvetech.validation.UserValidator.PASSWORD_LABEL;
import static io.ruvetech.validation.UserValidator.USERNAME_LABEL;
import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserValidatorTest {

    private final LocalDate dob = LocalDate.of(2000, 8, 23);
    @Mock
    private ConstraintValidatorContext context;
    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder violationBuilder;
    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext customizableContext;
    private UserValidator userValidator;
    private ApplicationUser applicationUser;

    @Before
    public void setUp() {
        userValidator = new UserValidator();
        given(context.buildConstraintViolationWithTemplate(anyString())).willReturn(violationBuilder);
        given(violationBuilder.addPropertyNode(anyString())).willReturn(customizableContext);
    }

    @Test
    public void testIsValidationFailedWhenUserObjectIsNull() {
        // object passed is null
        boolean isValid = userValidator.isValid(null, context);
        assertFalse("object passed is null, validation should have failed", isValid);
    }

    @Test
    public void testIsValidationFailedWhenUsernameIsNull() {
        applicationUser = new ApplicationUser(null, "1Passss", "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        then(violationBuilder).should().addPropertyNode(argument.capture());
        assertEquals("username is null, validation should have failed", USERNAME_LABEL, argument.getValue());
    }

    @Test
    public void testIsValidationFailedWhenUsernameAndPasswordIsNull() {
        applicationUser = new ApplicationUser(null, null, "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        List expected = asList(USERNAME_LABEL, PASSWORD_LABEL);
        then(violationBuilder).should(times(2)).addPropertyNode(argument.capture());
        assertEquals("username and password are null, validation should have failed", expected, argument.getAllValues());
    }

    @Test
    public void testIsValidationFailedWhenInvalidUsernameAndInvalidPassword() {
        applicationUser = new ApplicationUser("awe", "qwe", "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        List expected = asList(USERNAME_LABEL, PASSWORD_LABEL);
        then(violationBuilder).should(times(2)).addPropertyNode(argument.capture());
        assertEquals("username and password are invalid, validation should have failed", expected, argument.getAllValues());
    }


    @Test
    public void testIsValidationFailedWhenInvalidPasswordWithShortLength() {
        applicationUser = new ApplicationUser("awesome", "qwe", "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        then(violationBuilder).should().addPropertyNode(argument.capture());
        assertEquals("password supplied is short, validation should have failed", PASSWORD_LABEL, argument.getValue());
    }

    @Test
    public void testIsValidationFailedWhenInvalidPasswordNoUppercase() {
        applicationUser = new ApplicationUser("awesome", "qwer1", "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        then(violationBuilder).should().addPropertyNode(argument.capture());
        assertEquals("password supplied not have uppercase letter, validation should have failed", PASSWORD_LABEL, argument.getValue());
    }

    @Test
    public void testIsValidationFailedWhenInvalidPasswordNoNumber() {
        applicationUser = new ApplicationUser("awesome", "Password", "AAA-GG-SSSS", dob);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

        boolean isValid = userValidator.isValid(applicationUser, context);
        assertThat(isValid).isFalse();
        then(violationBuilder).should().addPropertyNode(argument.capture());
        assertEquals("password supplied not have a number, validation should have failed", PASSWORD_LABEL, argument.getValue());
    }

    @Test
    public void testIsValidationSuccessful() {
        ApplicationUser applicationUser1 = new ApplicationUser("username", "Pas1", "AAA-GG-SSSS", dob);
        ApplicationUser applicationUser2 = new ApplicationUser("username", "Password1", "AAA-GG-SSSS", dob);

        //4 character password with an uppercase letter and a number
        boolean isValid = userValidator.isValid(applicationUser1, context);
        assertThat(isValid).isTrue();

        // password with more than 4 characters and also include an uppercase letter and a number
        isValid = userValidator.isValid(applicationUser2, context);
        assertThat(isValid).isTrue();

        then(violationBuilder).should(never()).addPropertyNode(anyString());

    }

}
