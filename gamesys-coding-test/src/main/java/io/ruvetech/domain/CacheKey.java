package io.ruvetech.domain;

import java.util.Objects;

public final class CacheKey {

    private final String ssn;
    private final String dob;

    public CacheKey(String ssn, String dob) {
        this.ssn = ssn;
        this.dob = dob;
    }

    public String getSsn() {
        return ssn;
    }

    public String getDob() {
        return dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CacheKey cacheKey = (CacheKey) o;
        return ssn.equals(cacheKey.ssn) &&
                dob.equals(cacheKey.dob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ssn, dob);
    }
}
