package io.ruvetech.domain;

import io.ruvetech.validation.UserValidationInfo;

import java.time.LocalDate;

@UserValidationInfo
public class ApplicationUser {

    private String userName;

    private String password;

    private String ssnNumber;

    private LocalDate dob;

    public ApplicationUser() {
    }

    public ApplicationUser(String username, String password, String ssnNumber, LocalDate dob) {
        this.ssnNumber = ssnNumber;
        this.dob = dob;
        this.userName = username;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSsnNumber() {
        return ssnNumber;
    }

    public void setSsnNumber(String ssnNumber) {
        this.ssnNumber = ssnNumber;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }
}
