package io.ruvetech.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {UserValidator.class})
public @interface UserValidationInfo {

    String message() default "Invalid username/password";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
