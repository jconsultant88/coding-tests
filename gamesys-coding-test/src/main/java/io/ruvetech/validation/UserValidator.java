package io.ruvetech.validation;

import io.ruvetech.domain.ApplicationUser;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Validated
public class UserValidator implements ConstraintValidator<UserValidationInfo, ApplicationUser> {

    public static final String PASSWORD_NOT_VALID = "password not valid";
    static final String USERNAME_LABEL = "username";
    static final String PASSWORD_LABEL = "password";
    private static final String USERNAME_REGEX = "[a-zA-Z0-9]*";
    private static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[A-Z])[0-9a-zA-Z]{4,}$";
    private static final String USERNAME_REQUIRED = "username required";
    private static final String PASSWORD_REQUIRED = "password required";
    private static final String USERNAME_NOT_VALID = "username not valid";
    private static final int PASSWORD_SIZE = 4;
    private static final int USERNAME_SIZE = 4;

    @Override
    public boolean isValid(ApplicationUser applicationUser,
                           ConstraintValidatorContext context) {

        if (applicationUser == null) return false;

        boolean isValidUserName = false;
        boolean isValidPassword = false;

        String username = applicationUser.getUserName();

        if (StringUtils.isEmpty(username)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(USERNAME_REQUIRED)
                    .addPropertyNode(USERNAME_LABEL).addConstraintViolation();
        } else {
            isValidUserName = username.length() >= USERNAME_SIZE && username.matches(USERNAME_REGEX);
            if (!isValidUserName) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(USERNAME_NOT_VALID)
                        .addPropertyNode(USERNAME_LABEL).addConstraintViolation();
            }
        }
        String password = applicationUser.getPassword();
        if (StringUtils.isEmpty(password)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(PASSWORD_REQUIRED)
                    .addPropertyNode(PASSWORD_LABEL).addConstraintViolation();
        } else {
            isValidPassword = password.length() >= PASSWORD_SIZE && password.matches(PASSWORD_REGEX);
            if (!isValidPassword) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(PASSWORD_NOT_VALID)
                        .addPropertyNode(PASSWORD_LABEL).addConstraintViolation();
            }
        }
        return isValidUserName && isValidPassword;
    }
}
