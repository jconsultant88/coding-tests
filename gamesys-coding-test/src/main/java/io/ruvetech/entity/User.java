package io.ruvetech.entity;

import java.time.LocalDate;

public class User {

    private String username;

    private String password;

    private String ssnNumber;

    private LocalDate dob;

    public User() {
    }

    public User(String username, String password, String ssnNumber, LocalDate dob) {
        this.ssnNumber = ssnNumber;
        this.dob = dob;
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSsnNumber() {
        return ssnNumber;
    }

    public void setSsnNumber(String ssnNumber) {
        this.ssnNumber = ssnNumber;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }
}
