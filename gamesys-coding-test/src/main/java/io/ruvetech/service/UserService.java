package io.ruvetech.service;

import io.ruvetech.domain.ApplicationUser;
import io.ruvetech.domain.CacheKey;
import io.ruvetech.entity.User;
import io.ruvetech.exception.UserAlreadyRegisteredException;
import io.ruvetech.exception.UserBlackListedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

import static io.ruvetech.config.AppConfig.USER_CACHE;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final CacheManager cacheManager;

    private final ExclusionService exclusionService;

    private Cache userCache;

    public UserService(ExclusionService exclusionService,
                       CacheManager cacheManager) {
        this.exclusionService = exclusionService;
        this.cacheManager = cacheManager;
    }

    @PostConstruct
    public void init() {
        userCache = cacheManager.getCache(USER_CACHE);
    }

    public void createUser(ApplicationUser applicationUser) {

        boolean isUserNotBlackListed = exclusionService.validate(applicationUser.getSsnNumber(), applicationUser.getDob().toString());

        if (!isUserNotBlackListed) {
            throw new UserBlackListedException("User cannot be registered");
        }
        User existingUser = getUser(applicationUser.getSsnNumber(), applicationUser.getDob());
        if (existingUser != null) {
            LOGGER.info(String.format("User '%s' already exists", applicationUser.getUserName()));
            throw new UserAlreadyRegisteredException("User exists with given ssn and dob");
        }
        User newUser = new User(applicationUser.getUserName(), applicationUser.getPassword(),
                applicationUser.getSsnNumber(), applicationUser.getDob());
        userCache.put(new CacheKey(newUser.getSsnNumber(), newUser.getDob().toString()), newUser);

        LOGGER.info(String.format("User '%s' added to the cache", applicationUser.getUserName()));
    }

    public User getUser(String ssn, LocalDate dob) {
        CacheKey cacheKey = new CacheKey(ssn, dob.toString());
        return userCache.get(cacheKey, User.class);
    }
}
