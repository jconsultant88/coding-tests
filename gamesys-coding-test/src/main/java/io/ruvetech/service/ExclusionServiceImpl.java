package io.ruvetech.service;

import io.ruvetech.domain.CacheKey;
import io.ruvetech.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

import static io.ruvetech.config.AppConfig.EXCLUSION_CACHE;

@Service
public class ExclusionServiceImpl implements ExclusionService {

    @Autowired
    private CacheManager cacheManager;

    private Cache exclusionCache;

    @PostConstruct
    public void init() {
        exclusionCache = cacheManager.getCache(EXCLUSION_CACHE);
        User blackListedUser = new User("", "", "111-11-1111",
                LocalDate.of(1980, 1, 1));
        CacheKey cacheKey = new CacheKey(blackListedUser.getSsnNumber(), blackListedUser.getDob().toString());
        exclusionCache.put(cacheKey, blackListedUser);
    }


    @Override
    public boolean validate(String ssn, String dob) {
        CacheKey cacheKey = new CacheKey(ssn, dob);
        User userToExclude = exclusionCache.get(cacheKey, User.class);
        return userToExclude == null;
    }
}
