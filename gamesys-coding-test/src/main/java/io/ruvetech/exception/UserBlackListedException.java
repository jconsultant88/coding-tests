package io.ruvetech.exception;

public class UserBlackListedException extends RuntimeException {

    public UserBlackListedException(String message) {
        super(message);
    }
}
