package io.ruvetech.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.ruvetech.domain.ApplicationUser;
import io.ruvetech.domain.ValidationError;
import io.ruvetech.exception.UserAlreadyRegisteredException;
import io.ruvetech.exception.UserBlackListedException;
import io.ruvetech.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class ApplicationUserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationUserController.class);

    private final UserService userService;

    public ApplicationUserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity register(@Valid @RequestBody ApplicationUser applicationUser) {
        userService.createUser(applicationUser);
        LOGGER.info("User created");
        return ResponseEntity.created(URI.create("/register")).build();
    }

    @ExceptionHandler(UserAlreadyRegisteredException.class)
    private ResponseEntity<ValidationError> userAlreadyRegistered() {
        LOGGER.error("User already registered");
        return ResponseEntity.badRequest().body(new ValidationError("user already registered"));
    }

    @ExceptionHandler(UserBlackListedException.class)
    private ResponseEntity<ValidationError> userBlackListed(UserBlackListedException exception) {
        LOGGER.error("User blacklisted");
        return ResponseEntity.badRequest().body(new ValidationError(exception.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationError> handleException(MethodArgumentNotValidException exception) {
        LOGGER.error("Validation failed");
        String errorMessage = "Validation failed. " + exception.getBindingResult().getErrorCount() + " error(s)";
        ValidationError validationError = new ValidationError(errorMessage);
        for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
            validationError.addValidationError(fieldError.getDefaultMessage());
        }
        return ResponseEntity.badRequest().body(validationError);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ValidationError> handleException(HttpMessageNotReadableException exception) {
        LOGGER.error("Could not convert to required type");
        String errorMessage = "Bad request";
        if (exception.getCause() instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) exception.getCause();
            errorMessage = "Could not parse " + "'" + invalidFormatException.getValue() + "'" + " to " + invalidFormatException.getTargetType().getName();
        }
        return ResponseEntity.badRequest().body(new ValidationError(errorMessage));
    }

}
