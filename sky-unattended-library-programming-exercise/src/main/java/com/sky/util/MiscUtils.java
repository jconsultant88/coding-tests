package com.sky.util;

import java.util.Arrays;
import java.util.stream.Collectors;

import static java.util.Optional.*;

public class MiscUtils {


    public static String trimStringTo(final String str, final int noOFWords, final String postfix) {

        String[] strArray = ofNullable(str)
                .map(s -> s.split(" "))
                .orElse(new String[0]);

        if (strArray.length > 9) {
            return Arrays.stream(strArray)
                    .filter(string -> !string.isEmpty())
                    .limit(noOFWords)
                    .collect(Collectors.joining(" ", "", postfix));
        }
        return str;
    }
}
