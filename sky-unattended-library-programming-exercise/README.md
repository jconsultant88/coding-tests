### Assumptions
Assumed that files in the package 'com.sky.library' are not be modified

### Tools and java version used
* Java 8
* Intellij
* Maven

### Dependencies
- Junit library with 'test' scope

### Test Coverage
Only 'acceptance criteria' tests are covered in unit tests.
Could not further extend tests to cover 'utility' methods due to time constraint