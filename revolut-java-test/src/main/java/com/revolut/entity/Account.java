package com.revolut.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "ACCOUNTS")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Account implements VersionedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Column(name = "accountName")
    private String accountName;

    @Column(name = "customerId")
    private long customerId;

    @Version
    @Column(name = "version")
    private long version;

    @Column
    private byte type;

    @Column
    private BigDecimal amount;

    @Override
    public Long getId(){
        return id;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    @Override
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String getTableName() {
        return "ACCOUNTS";
    }
}
