package com.revolut.entity;

public interface VersionedEntity {

    Long getId();

    Long getVersion();

    void setVersion(Long version);

    String getTableName();
}
