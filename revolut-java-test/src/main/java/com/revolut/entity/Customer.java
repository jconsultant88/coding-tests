package com.revolut.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "CUSTOMER")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Customer {

    @Id
    @Column
    private long id;

    @Column
    private String name;

    @Column
    private byte state;

    @Column
    private int version;
}
