package com.revolut.service;


import com.revolut.data.TransferRequest;
import com.revolut.entity.Account;
import com.revolut.repository.AccountRepository;
import com.revolut.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Slf4j
public class TransferService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Transactional
    public void transfer(TransferRequest transferRequest) {

        customerRepository.findOne(transferRequest.getCustomerId()).
                orElseThrow(() -> new EntityNotFoundException("" + transferRequest.getCustomerId()));

        Account sourceAccount = accountRepository.findByCustomerIdAndAccountNumber(transferRequest.getCustomerId(),
                transferRequest.getSourceAccountId()).
                orElseThrow(() -> new EntityNotFoundException("" + transferRequest.getSourceAccountId()));

        Account destinationAccount = accountRepository.findByCustomerIdAndAccountNumber(transferRequest.getCustomerId(),
                transferRequest.getDestinationAccountId()).
                orElseThrow(() -> new EntityNotFoundException("" + transferRequest.getDestinationAccountId()));

        int diff = sourceAccount.getAmount().compareTo(transferRequest.getAmount());
        if (diff < 0) {
            throw new RuntimeException("No sufficient funds for account " + sourceAccount.getId());
        }
        BigDecimal newSourceAmount = sourceAccount.getAmount().subtract(transferRequest.getAmount());
        BigDecimal newDestinationAmount = destinationAccount.getAmount().add(transferRequest.getAmount());

        sourceAccount.setAmount(newSourceAmount);
        destinationAccount.setAmount(newDestinationAmount);

        accountRepository.save(sourceAccount);
        accountRepository.save(destinationAccount);

    }

}
