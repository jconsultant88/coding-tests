package com.revolut.annotations;

import java.lang.annotation.*;

/**
 * Marks an Entity for optimistic locking.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OptimisticlyLocked {

}
