package com.revolut.controller;

import com.revolut.dto.AccountTransferDto;
import com.revolut.service.TransferService;
import com.revolut.util.Converters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TransferController {

    @Autowired
    private TransferService transferService;

    @PostMapping("/transfer")
    public void transfer(@RequestBody AccountTransferDto accountTransferDto) {
        transferService.transfer(Converters.convert(accountTransferDto));
    }
}
