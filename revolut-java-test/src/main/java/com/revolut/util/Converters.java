package com.revolut.util;

import com.revolut.data.TransferRequest;
import com.revolut.dto.AccountTransferDto;

import java.math.BigDecimal;

public class Converters {

    private Converters() {

    }

    public static TransferRequest convert(AccountTransferDto accountTransferDto) {
//        long customerid = Long.parseLong(accountTransferDto.getCustomerId());
//        long sAccount = Long.parseLong(accountTransferDto.getDestinationAccountId());
//        long dAccount = Long.parseLong(accountTransferDto.getSourceAccountId());
        return new TransferRequest(Long.parseLong(accountTransferDto.getCustomerId()),
                Long.parseLong(accountTransferDto.getSourceAccountId()),
                Long.parseLong(accountTransferDto.getDestinationAccountId()),
                (new BigDecimal(accountTransferDto.getAmount())));

    }
}
