package com.revolut.repository;

import com.revolut.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CustomerRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Optional<Customer> findOne(Long id) {
        return this.jdbcTemplate.queryForObject("select * from customer where id = ?",
                new Object[]{id},
                (rs, rowNum) -> {
                    Customer customer = new Customer();
                    customer.setId(rs.getLong("id"));
                    customer.setName(rs.getString("name"));
                    return Optional.of(customer);
                });
    }
}
