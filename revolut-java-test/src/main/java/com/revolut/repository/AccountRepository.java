package com.revolut.repository;

import com.revolut.annotations.OptimisticlyLocked;
import com.revolut.entity.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Slf4j
@Repository
public class AccountRepository {

    @SuppressWarnings("unused")
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Optional<Account> findByCustomerIdAndAccountNumber(Long customerId, Long accountNumber) {
        return this.jdbcTemplate.queryForObject("select version, accountName, type, amount from accounts where customerId = ? and id = ?",
                new Object[]{customerId, accountNumber},
                (rs, rowNum) -> {
                    Account account = new Account();
                    account.setVersion(rs.getLong("version"));
                    account.setAccountName(rs.getString("accountName"));
                    account.setType(rs.getByte("type"));
                    account.setCustomerId(customerId);
                    account.setId(accountNumber);
                    account.setAmount(rs.getBigDecimal("amount"));
                    return Optional.of(account);
                });
    }

    @OptimisticlyLocked
    public Optional<Account> save(Account account) {
        this.jdbcTemplate.update("update accounts set version = ? , amount = ? where customerId = ? and id = ?",
                account.getVersion(), account.getAmount(), account.getCustomerId(), account.getId());

        return this.findByCustomerIdAndAccountNumber(account.getCustomerId(), account.getId());
    }

}