package com.revolut.data;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferRequest {

    private long customerId;

    private long sourceAccountId;

    private long destinationAccountId;

    private BigDecimal amount;
}
