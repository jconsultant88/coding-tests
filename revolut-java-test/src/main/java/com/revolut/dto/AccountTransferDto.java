package com.revolut.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class AccountTransferDto {

    private String customerId;

    private String sourceAccountId;

    private String destinationAccountId;

    private String amount;

}
