# Getting Started

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Optimistically Locking Your Spring Boot Web Services](https://medium.com/slalom-engineering/optimistically-locking-your-spring-boot-web-services-187662eb8a91)