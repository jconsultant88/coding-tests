package io.ruvetech;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {

    public static List<String> readFile(String fileName) {
        boolean fileExists = new File(fileName).exists();
        if (!fileExists) {
            throw new RuntimeException("No such file : " + fileName);
        }
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            return stream.collect(Collectors.toCollection(ArrayList::new));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
