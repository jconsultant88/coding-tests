package io.ruvetech;

import java.util.Stack;

import static io.ruvetech.SupportedOperations.*;
import static java.lang.Double.parseDouble;

public class RPNEngine {

    private RPNValidator rpnValidator = new RPNValidator();

    /**
     * Computes the outcome of a given expression in Reverse Polish Notation
     *
     * @param expr the expression to compute
     */
    public double compute(String expr) {
        Stack<Double> stack = new Stack<>();

        if (!rpnValidator.apply(expr)) {
            throw new RuntimeException("Illegal RPN");
        }

        String tokens[] = expr.split("\\s+");

        for (String token : tokens) {
            switch (token) {
                case "+":
                    stack.push(ADD.apply(stack.pop(), stack.pop()));
                    break;
                case "-":
                    Double operand2 = stack.pop();
                    stack.push(SUB.apply(stack.pop(), operand2));
                    break;
                case "*":
                    stack.push(MUL.apply(stack.pop(), stack.pop()));
                    break;
                case "/":
                    operand2 = stack.pop();
                    stack.push(DIV.apply(stack.pop(), operand2));
                    break;
                case "mod":
                    operand2 = stack.pop();
                    stack.push(MOD.apply(stack.pop(), operand2));
                    break;
                case "sin":
                    stack.push(SIN.apply(stack.pop()));
                    break;
                case "cos":
                    stack.push(COS.apply(stack.pop()));
                    break;
                case "sqrt":
                    stack.push(SQRT.apply(stack.pop()));
                    break;
                default:
                    stack.push(parseDouble(token));
                    break;
            }
        }
        return stack.pop();
    }

}