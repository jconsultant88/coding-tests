package io.ruvetech;


import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class SupportedOperations {

    final static UnaryOperator<Double> SIN = d -> Math.sin(d);

    final static UnaryOperator<Double> COS = d -> Math.cos(d);

    final static UnaryOperator<Double> SQRT = Math::sqrt;

    final static BinaryOperator<Double> AVG = (d1, d2) -> (d1 + d2) / 2;

    final static BinaryOperator<Double> MOD = (d1, d2) -> d1 % d2;

    final static BinaryOperator<Double> ADD = (d1, d2) -> d1 + d2;

    final static BinaryOperator<Double> SUB = (d1, d2) -> d1 - d2;

    final static BinaryOperator<Double> MUL = (d1, d2) -> d1 * d2;

    final static BinaryOperator<Double> DIV = (d1, d2) -> d1 / d2;

}

