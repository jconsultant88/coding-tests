package io.ruvetech;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.function.Function;

public class RPNValidator implements Function<String, Boolean> {

    public static final List<String> BOPERATION = Arrays.asList("+", "-", "*", "/", "avg", "mod");

    public static final List<String> UOPERATION = Arrays.asList("sqrt", "sin", "cos");

    /**
     * Validates a  Reverse Polish Notation expression
     *
     * @param expr the expression to compute
     */
    @Override
    public Boolean apply(String expr) {
        Stack<String> stack = new Stack<>();

        String tokens[] = expr.split("\\s+");


        if (tokens.length < 2) {
            return false;
        }
        for (String token : tokens) {
            if (BOPERATION.contains(token)) {
                if (stack.size() == 2) {
                    stack.pop();
                } else {
                    return false;
                }
            } else if (UOPERATION.contains(token)) {
                if (!(stack.size() == 1)) {
                    return false;
                }
            } else {
                stack.push(token);
            }
        }
        return stack.size() == 1;
    }
}