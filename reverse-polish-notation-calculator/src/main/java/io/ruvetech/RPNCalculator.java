package io.ruvetech;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RPNCalculator {

    /**
     * Runs the calculation for the RPNEngine expression in args[0].
     */
    public static void main(String[] args) {

        RPNEngine rpnEngine = new RPNEngine();

        String fileName = null;
        if (args.length > 0) {
            fileName = args[0];
        } else {
            fileName = "src/main/resources/data.txt";
        }

        List<String> stringList = Optional.ofNullable(FileUtils.readFile(fileName)).orElseThrow(
                RuntimeException::new);

        String newStringData = stringList.stream()
                .map(stringData -> {
                    try {
                        double result = rpnEngine.compute(stringData);
                        return stringData + " = " + result;
                    } catch (RuntimeException e) {
                        //invalid RPN
                        return stringData + " - Not Reverse Polish Notation try backwards ";
                    }
                })
                .collect(Collectors.joining("\n"));

        System.out.println(newStringData);
    }
}
