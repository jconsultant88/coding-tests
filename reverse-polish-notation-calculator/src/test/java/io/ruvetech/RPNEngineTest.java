package io.ruvetech;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RPNEngineTest {

    private RPNEngine rpnEngine;

    @Before
    public void beforeEveryTest(){
        rpnEngine = new RPNEngine();
    }

    @Test
    public void testSuccessfulExpressions() {

        assertEquals(3.0, rpnEngine.compute("1.0 2.0 +"), 0.0);
        assertEquals(12.0, rpnEngine.compute("3 4 *"), 0.0);
        assertEquals(4.0, rpnEngine.compute("6 3 * 2 - sqrt"), 0.0);
        assertEquals(0.0, rpnEngine.compute("4 2 - 2 - 1000 *"), 0.0);

    }

    @Test(expected = RuntimeException.class)
    public void testRuntimeException() {
        rpnEngine.compute("3 4");
        fail("Should not have reached this point");
    }
}
