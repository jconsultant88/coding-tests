package io.ruvetech;


import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RPNValidatorTest {

    @Test
    public void validateAll() {

        Function<String, Boolean> rplValidator = new RPNValidator();

        assertFalse(rplValidator.apply(""));
        assertFalse(rplValidator.apply("2 *"));
        assertFalse(rplValidator.apply("- 4 2"));
        assertFalse(rplValidator.apply("1 + 1"));
        assertFalse(rplValidator.apply("4 2 - 2"));
        assertFalse(rplValidator.apply("4 2 - 2 - 1000"));
        assertFalse(rplValidator.apply("sqrt"));
        assertFalse(rplValidator.apply("sqrt 4"));
        assertFalse(rplValidator.apply("90 tan"));
        assertFalse(rplValidator.apply("tan tan"));
        assertFalse(rplValidator.apply("6 3 * 2 - sqrt 2 + sin avg"));

        assertTrue(rplValidator.apply("3 4 *"));
        assertTrue(rplValidator.apply("3 4 +"));
        assertTrue(rplValidator.apply("3 4 -"));
        assertTrue(rplValidator.apply("3 4 /"));
        assertTrue(rplValidator.apply("4 sqrt"));
        assertTrue(rplValidator.apply("4 sin"));
        assertTrue(rplValidator.apply("90 sin 3 *"));
        assertTrue(rplValidator.apply("4 2 - 2 -"));
        assertTrue(rplValidator.apply("16 sqrt sqrt"));
        assertTrue(rplValidator.apply("6 3 * 2 - sqrt"));
        assertTrue(rplValidator.apply("6 3 * 2 - sqrt 2 +"));
        assertTrue(rplValidator.apply("4 2 - 2 - 1000 *"));

        assertTrue(rplValidator.apply("6 3 * 2 - sqrt 2 + sin"));
        assertTrue(rplValidator.apply("6 3 * 2 - sqrt 2 + sin cos"));
        assertTrue(rplValidator.apply("6 3 * 2 - sqrt 2 + sin 2 avg"));
        assertTrue(rplValidator.apply("6 3 * 2 - sqrt 2 + sin 2 mod"));
        assertTrue(rplValidator.apply("4 2 - 2 - 1000 * cos"));

        assertTrue(rplValidator.apply("3.0 4.0 *"));
        assertTrue(rplValidator.apply("4.0 sqrt"));
        assertTrue(rplValidator.apply("4.0 2.0 - 2.0 -"));
        assertTrue(rplValidator.apply("16.0 sqrt sqrt"));


        assertTrue(rplValidator.apply("4 sqrt sqrt sqrt"));
    }
}
