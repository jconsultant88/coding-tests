package io.ruvetech;

import org.junit.Test;

import static io.ruvetech.SupportedOperations.*;
import static org.junit.Assert.assertEquals;

public class SupportedOperationsTest {

    @Test
    public void testAllFunctions() {
        assertEquals(-0.448, COS.apply(90.0), 0.001);
        assertEquals(0.89, SIN.apply(90.0), 0.007);
        assertEquals(6.0, SQRT.apply(36.0), 0.0);

        assertEquals(1.50, AVG.apply(1.0, 2.0), 0.0);
        assertEquals(0, MOD.apply(4.0, 2.0), 0.0);
        assertEquals(3.0, ADD.apply(1.0, 2.0), 0.0);
        assertEquals(1.0, SUB.apply(2.0, 1.0), 0.0);
        assertEquals(4.00, MUL.apply(2.0, 2.0), 0.0);
        assertEquals(0.50, DIV.apply(1.0, 2.0), 0.0);

    }
}
