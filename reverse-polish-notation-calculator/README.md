### Introduction
This is a maven project aimed to solve the assignment for capgemini

##### Tools and technologies used
* Java 8
* Intellij
* Maven

##### Dependencies
* junit

##### Running the program
If its is first time then run the following command to

```sh
$ <go to the project path>
$ mvn clean test
$ mvn exec:java -Dexec.mainClass="io.ruvetech.RPNCalculator" -Dexec.args="src/main/resources/data.txt"
``` 

##### Some information
Junits are not covered for RPNCalculator and FileUtils because of time constraint.