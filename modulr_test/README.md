### How we use this

We use an external platform for the coding challenge, but the test is bespoke to Modulr.  If you have any problems, please get in touch with us directly and we can investigate.  If you can’t complete the challenge, don’t panic, we’re interested in quality and not quantity.  

### What to expect

You’ll get a separate email from Geektastic with a link to the coding challenge.  So that you feel a bit more prepared, I’ve copied and pasted some of the instructions here.  Please review these in advance.

### Summary

You are given code for two financial services which contain several defects. You need to identify and fix the defects.

### Prerequisites

If you are working on your own machine you will need the following:

    You will need a location in which you can work undisturbed at a computer for up to 90-minutes
    You will need to have a development environment set up with a plain Java 8 or later JDK configured.
    Your environment will need to include Maven 3 and an internet connection to pull down other dependencies.

The test involves completing working code in Java. When you choose to start you will be provided with instructions and links to download code files. You will be expected to upload your completed code compressed using .zip at the end of the test period.

### Instructions

You are provided with some code for two financial services (use the download button above to get the starting code as a ZIP file), but unfortunately the code contains some defects.

Your objectives are:

    Identify and fix the defects.
    Ensure that all requirements are effectively tested.

Some JUnit-based tests are included with the supplied code. The supplied unit tests themselves do NOT contain defects BUT they may be incomplete or insufficient to validate all the requirements for the services which are listed below.

We have also provided a markdown file (`src/main/resources/comments.md`). Please use it to document any assumptions that you have made.

The code is for two financial services: an `AccountService` and an `ATMService`. The code was created to attempt to meet the following requirements.

#### `AccountService`

    Checks the account balance
    Withdraws an amount of money from the account

#### `ATMService`

Should have the following behaviour

    Replenish:
        Sets up the service with currency notes of denominations 5, 10, 20 and 50
    Check balance:
        Returns a formatted balance String to display to the user
    Withdraw:
        Returns notes of the correct denominations for the requested withdrawal amount
        Allow withdrawals between 20 and 250 inclusive
        Disburse smallest number of notes possible
        Always disburse at least one 5 note, if possible

#### Notes:

    It is acceptable to disregard currency for all operations.

FAQs

If any of the following happen:

    You run out of time or there is an issue with the internet
    The upload fails
    There is a problem with your internet connection while doing the test.

Please save the code preserving this structure of the zip file and the names of the files then get in touch with us.