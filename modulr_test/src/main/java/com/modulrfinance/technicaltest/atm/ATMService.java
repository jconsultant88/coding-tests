package com.modulrfinance.technicaltest.atm;

import com.modulrfinance.technicaltest.account.AccountService;
import com.modulrfinance.technicaltest.money.Denomination;
import com.modulrfinance.technicaltest.money.MoneyAsNotes;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

class ATMService {

    private AccountService accountService;
    private MoneyAsNotes vault;

    ATMService(AccountService accountService, MoneyAsNotes vault) {
        this.accountService = accountService;
        this.vault = vault;
    }

    String checkBalance(String accountNumber) {
        BigDecimal balance = accountService.checkBalance(accountNumber);
        return NumberFormat.getCurrencyInstance(Locale.UK).format(balance);
    }

    void replenish(MoneyAsNotes moneyAsNotes) {
        vault = vault.add(moneyAsNotes);
    }

    MoneyAsNotes withdraw(String accountNumber, BigDecimal amount) {
        MoneyAsNotes withdrawalPool = prepareMoney(amount);
        accountService.withdraw(accountNumber, amount);
        vault = vault.remove(withdrawalPool);
        return withdrawalPool;
    }

    private MoneyAsNotes prepareMoney(BigDecimal amount) {
        MoneyAsNotes moneyAsNotes = prepareMoneyStep(amount, vault);
        if (moneyAsNotes == null) {
            throw new NotEnoughNotesException();
        }
        //TODO - Added this check to fix the code
        if (!checkIfMoneyDispensedAsPerPriority(moneyAsNotes)) {
            moneyAsNotes = dispensedAsPerPriority(moneyAsNotes);
        }
        return moneyAsNotes;
    }

    private MoneyAsNotes prepareMoneyStep(BigDecimal amount, MoneyAsNotes remainingVault) {
        if (amount.compareTo(BigDecimal.ZERO) == 0) {
            return MoneyAsNotes.createEmpty();
        }
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            return null;
        }
        for (Denomination denomination : Denomination.valuesDescending()) {
            if (remainingVault.get(denomination) > 0) {
                BigDecimal remainingAmount = amount.subtract(denomination.value());
                MoneyAsNotes moneyAsNotes = prepareMoneyStep(remainingAmount, remainingVault.remove(denomination, 1));
                if (moneyAsNotes != null) {
                    return moneyAsNotes.add(denomination, 1);
                }
            }
        }
        return null;
    }

    private List<Denomination> getPriorityDenominations() {
        List<Denomination> priorityList = new ArrayList<>();
        priorityList.add(Denomination.FIVE);
        //priorityList.add(Denomination.TEN);

        return priorityList;
    }

    private boolean checkIfMoneyDispensedAsPerPriority(MoneyAsNotes dispensedNotes) {
        List<Denomination> priorityList = getPriorityDenominations();
        for (Denomination key : priorityList) {
            if (dispensedNotes.get(key) == 0) {
                return false;
            }
        }
        return true;
    }

    private MoneyAsNotes dispensedAsPerPriority(MoneyAsNotes dispensedNotes) {
        List<Denomination> priorityList = getPriorityDenominations();
        // sort denominations in ascending order
        //exclude priority denominations
        List<Denomination> denominationList = Denomination.valuesDescending().stream().
                sorted(Comparator.reverseOrder()).
                filter((element) -> !priorityList.contains(element)).
                collect(Collectors.toCollection(ArrayList::new));

        for (Denomination priorityDenomination : priorityList) {
            dispensed:
            for (Denomination denomination : denominationList) {
                //check if current denomination being looped is dispensed and
                // if it could be replaced with priority notes
                if (dispensedNotes.get(denomination) >= 1 && isRemainderZero(denomination.value(), priorityDenomination.value())) {
                    BigDecimal minQuantity = denomination.value().divide(priorityDenomination.value());
                    if (vault.get(priorityDenomination) >= minQuantity.longValue()) {
                        dispensedNotes = dispensedNotes.add(priorityDenomination, minQuantity.longValue());
                        dispensedNotes = dispensedNotes.remove(denomination, 1);
                        break dispensed;
                    }
                }
                continue;
            }
        }
        return dispensedNotes;
    }

    private boolean isRemainderZero(BigDecimal dividend, BigDecimal divisor) {
        return (dividend.remainder(divisor)).compareTo(BigDecimal.ZERO) == 0;
    }
}
