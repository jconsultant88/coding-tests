# Comments and assumptions
If you have any, place them in here

## Assumptions
- minimal code changes are to be made and the emphasis is given to extendability and maintainability and not to efficiency
- Current dispensing logic already present is correct and only the requirement that a minimum of one 5.00 denomination should be available at final dispense needs to be taken care
- Based on the above logic, failing test 'prioritiesFivesWhenDispensingMoney' is fixed
- configuration similar to 'atleast one 5.00' needs to be dispensed could be extended to other denominations on the fly

## Comments
- more test cases could not be added due to time constraints
- atm dispense strategy is not clear, making certain assumptions is breaking other unit tests so fix provided is mainly aimed to 'prioritiesFivesWhenDispensingMoney' unit test
